package com.example.demo.service.intf;

import com.example.demo.model.User;

public interface UserService {
    public User getById(Integer id);
}

