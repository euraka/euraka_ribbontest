package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.demo.model.User;

@RestController
public class UserController {
	@Autowired
    private RestTemplate restTemplate;
	@RequestMapping("user/get/{id}")
	 public User get(@PathVariable("id") Integer id) throws Exception {
		return this.restTemplate.getForObject("http://USER-API/provider/user/get/" + id, User.class);
}
}